<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

// phpcs:disable PSR1.Classes.ClassDeclaration.MissingNamespace

use BadPixxel\Robo\Extras\Console\ResultsBlock;
use Robo\Symfony\ConsoleIO;
use Robo\Tasks;

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see https://robo.li/
 */
class RoboFile extends Tasks
{
    /**
     * @command demo
     *
     * @description Bash Outputs Tests & Démo
     *
     * @return void
     */
    public function demo(ConsoleIO $consoleIo)
    {
        //====================================================================//
        // Console results Blocks
        $this->demoResultsBlock($consoleIo);
    }

    /**
     * @description Bash Outputs Tests & Démo
     *
     * @return void
     */
    private function demoResultsBlock(ConsoleIO $consoleIo)
    {
        $blocks = new ResultsBlock($consoleIo);
        //====================================================================//
        // Simple Notifications
        $consoleIo->title("Result Blocks - Simple");
        $blocks->result(true, "This is a Success!");
        $blocks->result(null, "This is a Warning!");
        $blocks->result(false, "This is an Error!");
        //====================================================================//
        // Task Results
        $blocks->results(array(
            "This is a Success!" => true,
            "This is a Warning!" => null,
            "This is a Error!" => false,
        ), "Result Blocks - Grouped");
    }
}
