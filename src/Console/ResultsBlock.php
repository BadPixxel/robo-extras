<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Extras\Console;

use Robo\Symfony\ConsoleIO;

/**
 * Render Console Results Blocks
 */
class ResultsBlock
{
    /**
     * @var ConsoleIO
     */
    private ConsoleIO $consoleIo;

    public function __construct(ConsoleIO $consoleIo)
    {
        $this->consoleIo = $consoleIo;
    }

    /**
     * Add Multiple Results block of text.
     *
     * @param array<string, ?scalar> $results
     * @param null|string            $title
     *
     * @return void
     */
    public function results(array $results, string $title = null): void
    {
        if ($title) {
            $this->consoleIo->section($title);
        }
        foreach ($results as $message => $state) {
            if (is_null($state) || is_bool($state)) {
                $this->result($state, $message);
            }
        }
    }

    /**
     * Add a Result block of text.
     *
     * @param bool   $state
     * @param string $messages
     *
     * @return void
     */
    public function result(?bool $state, string $messages): void
    {
        if ($state) {
            $this->success($messages);
        } elseif (null === $state) {
            $this->warning($messages);
        } else {
            $this->error($messages);
        }
    }

    /**
     * Add a Success Result block of text.
     *
     * @param string $messages
     *
     * @return void
     */
    public function success(string $messages): void
    {
        $this->consoleIo->writeln(sprintf('[<info> OK </info>] %s', $messages));
    }

    /**
     * Add an Error Result block of text.
     *
     * @param string $messages
     *
     * @return void
     */
    public function error(string $messages): void
    {
        $this->consoleIo->writeln(sprintf('[ <error>KO</error> ] %s', $messages));
    }

    /**
     * Add a Warning Result block of text.
     *
     * @param string $messages
     *
     * @return void
     */
    public function warning(string $messages): void
    {
        $this->consoleIo->writeln(sprintf('[<comment>WARN</comment>] %s', $messages));
    }
}
